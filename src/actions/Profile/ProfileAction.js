import axios from "axios";

export const CREATE_CREDENTIAL_PROFILE = (authToken, username, profileId) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.post(process.env.REACT_APP_PROFILE_SERVER + username,
                {"ProfileIdentity": profileId},
                {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result)
                })
                .catch((err) => {
                    reject(err)
                });
        });
    };
};