import axios from "axios";

export const SIGNIN = (accessToken) => ({
    type: "SIGN-IN",
    accessToken
});

export const SIGNOUT = () => ({
    type: "SIGN-OUT"
});

export const AUTHENTICATE_WITH_API = (username, password) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.post(process.env.REACT_APP_AUTH_SERVER, {
                UserName: username,
                Password: password,
            })
                .then((result) => {
                    const accessToken = result.data.token.result;
                    dispatch(SIGNIN(accessToken));
                    localStorage.setItem("accessToken", accessToken);
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const CREATE_CREDENTIAL_WITH_API = (username, email, password) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.post(process.env.REACT_APP_CREDENTIAL_CREATION_SERVER, {
                UserName: username,
                Password: password,
                Email: email
            })
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_AUTHENTICATION_LOGS = (authToken, username) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_AUTHENTICATION_LOG_SERVER + username;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};