import axios from "axios";

export const SET_EMPLOYEES = (employees) => ({
    type: "SET_EMPLOYEES",
    employees
});

export const ADD_EMPLOYEE = (employee) => ({
    type: "ADD_EMPLOYEE",
    employee
});

export const REMOVE_EMPLOYEE = (id) => ({
    type: "REMOVE_EMPLOYEE",
    id
});

export const UPDATE_EMPLOYEE = (updates) => ({
    type: "UPDATE_EMPLOYEE",
    updates
});

export const CREATE_EMPLOYEE_API = (authToken, employee) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.post(process.env.REACT_APP_EMPLOYEE_SERVER,
                {...employee},
                {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_ALL_EMPLOYEES_API = (authToken) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.get(process.env.REACT_APP_EMPLOYEE_SERVER,
                {headers: {Authorization: `Bearer ${authToken}`}}
            )
                .then((result) => {
                    dispatch(SET_EMPLOYEES(result.data));
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_EMPLOYEE_BY_IDENTITY_NUMBER_API = (authToken, identityNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + identityNumber;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_ALL_MANAGERS_API = (authToken) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + "managers";
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_ALL_BY_DEPARTMENT = (authToken, departmentNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + "department" + "/" + departmentNumber;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_ALL_MANAGERS_BY_DEPARTMENT = (authToken, departmentNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + "department" + "/" + departmentNumber;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    const managers = result.data.filter((employee) => {
                        return employee.isManager === true;
                    });
                    resolve(managers);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_ALL_EMPLOYEES_BY_DEPARTMENT = (authToken, departmentNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + "department" + "/" + departmentNumber;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    const managers = result.data.filter((employee) => {
                        return employee.isManager !== true;
                    });
                    resolve(managers);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_EMPLOYEE_CONTACT_INFORMATION = (authToken, identityNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + "contact" + "/" + identityNumber;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_EMPLOYEE_BY_USERNAME = (authToken, username) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + "username" + "/" + username;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const UPDATE_EMPLOYEE_API = (authToken, updates) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + updates.id;
            axios.put(href, updates, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const REMOVE_EMPLOYEE_API = (authToken, id) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_EMPLOYEE_SERVER + id;
            axios.delete(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const FILTER_EMPLOYEE_BY_FIRST_NAME = (filter, employeeCollection) => {
    let result = employeeCollection.filter((employee) => {
        return employee.firstName.toLowerCase().includes(filter.toLowerCase());
    });
    return result;
};

export const FILTER_EMPLOYEE_BY_LAST_NAME = (filter, employeeCollection) => {
    let result = employeeCollection.filter((employee) => {
        return employee.lastName.toLowerCase().includes(filter.toLowerCase());
    });
    return result;
};

export const FILTER_EMPLOYEE_BY_HIRED_DATE = (startDate, endDate, employeeCollection) => {
    startDate = new Date(startDate);
    endDate = new Date(endDate);
    let result = employeeCollection.filter((employee) => {
        let hireDate = new Date(employee.hiredDate);
        return hireDate >= startDate && hireDate <= endDate;
    });
    return result;
};

export const FILTER_EMPLOYEE_BY_DEPARTMENT = (department, employeeCollection) => {
    let result = employeeCollection.filter((employee) => {
        return Number(employee.departmentForeignKey) === Number(department);
    });
    return result;
};

export const FILTER_EMPLOYEE_BY_EMPLOYMENT_STATUS = (status, employeeCollection) => {
    let result = employeeCollection.filter((employee) => {
        return employee.employmentStatus.toLowerCase() === status.toLowerCase();
    });
    return result;
};