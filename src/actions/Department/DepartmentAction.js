import axios from "axios";

export const SET_DEPARTMENTS = (departments) => {
    return {
        type: "SET_DEPARTMENTS",
        departments
    }
};

export const ADD_DEPARTMENT = (department) => {
    return {
        type: "ADD_DEPARTMENT",
        department
    }
};

export const REMOVE_DEPARTMENT = (departmentNumber) => {
    return {
        type: "REMOVE_DEPARTMENT",
        departmentNumber
    }
};

export const UPDATE_DEPARTMENT = (updates) => {
    return {
        type: "UPDATE_DEPARTMENT",
        updates
    }
};

export const CREATE_DEPARTMENT = (authToken, department) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.post(process.env.REACT_APP_DEPARTMENT_SERVER,
                department,
                {headers: {Authorization: `Bearer ${authToken}`}}
            )
                .then((result) => {
                    dispatch(ADD_DEPARTMENT(department));
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_ALL_DEPARTMENT = (authToken) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.get(
                process.env.REACT_APP_DEPARTMENT_SERVER,
                {headers: {Authorization: `Bearer ${authToken}`}}
            )
                .then((result) => {
                    dispatch(SET_DEPARTMENTS(result.data));
                    resolve(result.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_DEPARTMENT_BY_NUMBER = (authToken, departmentNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.get(process.env.REACT_APP_DEPARTMENT_SERVER + departmentNumber,
                {headers: {Authorization: `Bearer ${authToken}`}}
            )
                .then((result) => {
                    // dispatch(ADD_DEPARTMENT(result.data));
                    resolve(result.data);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const REMOVE_DEPARTMENT_WITH_API = (authToken, departmentNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const HREF = process.env.REACT_APP_DEPARTMENT_SERVER + departmentNumber;
            axios.delete(HREF,
                {headers: {Authorization: `Bearer ${authToken}`}}
            )
                .then((result) => {
                    dispatch(REMOVE_DEPARTMENT(departmentNumber));
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                })
        });
    };
};

export const UPDATE_DEPARTMENT_WITH_API = (authToken, updates) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.put(process.env.REACT_APP_DEPARTMENT_SERVER,
                updates,
                {headers: {Authorization: `Bearer ${authToken}`}}
            )
                .then((result) => {
                    dispatch(UPDATE_DEPARTMENT(updates));
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const GENERATE_DEPARTMENT_REPORT = (authToken, departmentNumber) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            axios.get(process.env.REACT_APP_DEPARTMENT_REPORT_SERVER + departmentNumber,
                {headers: {Authorization: `Bearer ${authToken}`}}
            )
                .then((result) => {
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const FILTER_DEPARTMENT_BY_NAME = (filter, departmentCollection) => {
    return (dispatch, getState) => {
        let result = departmentCollection.filter((department) => {
            return department.departmentName.toLowerCase().includes(filter.toLowerCase());
        });
        return result;
    };
};










