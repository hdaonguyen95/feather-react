import axios from "axios";

export const SET_PICTURES = (pictures) => ({
    type: "SET_PICTURES",
    pictures
});

export const ADD_PICTURE = (picture) => ({
    type: "ADD_PICTURE",
    picture
});

export const REMOVE_PICTURE = (id) => ({
    type: "REMOVE_PICTURE",
    id
});

export const UPLOAD_PICTURE = (authToken, username, body) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const formData = new FormData();
            formData.append("file", body);
            const href = process.env.REACT_APP_PICTURE_SERVER + "upload" + "/" + username;
            axios.post(href,
                formData,
                {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    dispatch(RETRIEVE_PICTURE(authToken, username, result.data.fileName));
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_PICTURE = (authToken, username, pictureId) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_PICTURE_SERVER + "retrieve" + "/" + username + "/" + pictureId;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then(result => {
                    dispatch(ADD_PICTURE(result.data));
                    resolve(result);
                })
                .catch(err => {
                    reject(err);
                });
        });
    };
};

export const RETRIEVE_ALL_PICTURES = (authToken, username) => {
    return (dispatch, getState) => {
        return new Promise((resolve, reject) => {
            const href = process.env.REACT_APP_PICTURE_SERVER + "retrieve" + "/" + username;
            axios.get(href, {headers: {Authorization: `Bearer ${authToken}`}})
                .then((result) => {
                    dispatch(SET_PICTURES(result.data));
                    resolve(result);
                })
                .catch((err) => {
                    reject(err);
                });
        });
    };
};