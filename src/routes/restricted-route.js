import React from "react";
import {Route, Redirect} from "react-router-dom";

export const RestrictedRoute = ({component: Component, ...rest}) => {
    const isAuthenticated = localStorage.getItem("accessToken");
    return (
        <Route {...rest} component={(props) => {
            return (!!!isAuthenticated ? <Component {...props}/> : <Redirect to="/profile"/>)
        }}/>
    );
}

export default RestrictedRoute;