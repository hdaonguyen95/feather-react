import React from "react";
import {Router, Switch, Route} from "react-router-dom";
import SignIn from "../components/Authentication/SignIn";
import PageNotFound from "../components/Error/PageNotFound";
import SignUp from "../components/Authentication/SignUp";
import Profile from "../components/Profile/Profile";
import Department from "../components/Department/Department";
import AllDepartment from "../components/Department/AllDepartment";
import AllEmployee from "../components/Employee/AllEmployee";
import Employee from "../components/Employee/Employee";
import AllManager from "../components/Employee/AllManager";
import {PrivateRoute} from "./private-route";
import RestrictedRoute from "./restricted-route";
import AllWithDepartment from "../components/Employee/AllWithDepartment";
import AllManagerWithDepartment from "../components/Employee/AllManagerWithDepartment";
import AllEmployeeWithDepartment from "../components/Employee/AllEmployeeWithDepartment";
import AuthenticationLogs from "../components/Logs/AuthenticationLogs";
import Pictures from "../components/Pictures/Pictures";
import About from "../components/About/About";

export const history = require("history").createBrowserHistory();

const PublicRoute = (props) => (
    <Router history={history}>
        <Switch>
            <PrivateRoute path={"/"} exact={true} component={Profile}/>
            <Route path={"/about"} exact={true} component={About} />
            <RestrictedRoute path={"/auth/sign-in"} exact={true} component={SignIn}/>
            <RestrictedRoute path={"/auth/sign-up"} exact={true} component={SignUp}/>
            <PrivateRoute path={"/profile"} exact={true} component={Profile}/>
            <PrivateRoute path={"/department/detail/:departmentNumber"} exact={true} component={Department}/>
            <PrivateRoute path={"/department/create/"} exact={true} component={Department}/>
            <PrivateRoute path={"/department"} exact={true} component={AllDepartment}/>
            <PrivateRoute path={"/employee/detail/:employee"} exact={true} component={Employee}/>
            <PrivateRoute path={"/employee/create"} exact={true} component={Employee}/>
            <PrivateRoute path={"/employee"} exact={true} component={AllEmployee}/>
            <PrivateRoute path={"/employee/manager"} exact={true} component={AllManager}/>
            <PrivateRoute path={"/employee/department/:department"} exact={true} component={AllWithDepartment}/>
            <PrivateRoute path={"/employee/manager/department/:department"} exact={true}
                          component={AllManagerWithDepartment}/>
            <PrivateRoute path={"/pictures"} exact={true} component={Pictures}/>
            <PrivateRoute path={"/authentication/authLogs"} exact={true} component={AuthenticationLogs}/>
            <PrivateRoute path={"/employee/normal/department/:department"} exact={true}
                          component={AllEmployeeWithDepartment}/>
            <Route component={PageNotFound}/>
        </Switch>
    </Router>
);

export default PublicRoute;