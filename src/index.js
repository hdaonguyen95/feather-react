import React, {useEffect} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '@fortawesome/fontawesome-free/css/all.min.css';
import
    'bootstrap-css-only/css/bootstrap.min.css';
import
    'mdbreact/dist/css/mdb.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Provider} from "react-redux";
import storeConfig from "./stores/config";
const store = storeConfig();

let rendered = false;
const renderRootComponent = () => {
    if (!rendered) {
        ReactDOM.render(
            <Provider store={store}><App/></Provider>,
            document.getElementById('root')
        );
        rendered = true;
    }
};

renderRootComponent();

reportWebVitals();