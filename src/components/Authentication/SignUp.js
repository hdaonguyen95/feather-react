import React from "react";
import {connect} from "react-redux";
import {Field, Form, withFormik} from "formik";
import * as yup from "yup";
import styles from "../../styles/Authentication/SignUp.module.css";
import ErrorModal from "../Modals/ErrorModal";
import {CREATE_CREDENTIAL_WITH_API} from "../../actions/Authentication/AuthAction";
import feather from "../../images/feather.png";
import Navigation from "../Navigation/Nagivation";

const SignUp = ({values, errors, touched, setFieldValue, isSubmitting}) => (
    <div>
        <Navigation/>
        <ErrorModal isOpen={values.errModal} toggle={() => {
            setFieldValue("errModal", !values.errModal);
        }} errorTitle={"Sign up failed"} errorMessage={values.signUpErr}/>
        <div className={styles.container}>
            <div className={styles.headerContainer}>
                <img src={feather} alt="feather"/>
                <h3>Sign Up</h3>
            </div>
            <div className={styles.main}>
                <Form>
                    {touched.username && errors.username && (
                        <div className={"form-group" + " " + styles.errorContainer}>
                            <p>{errors.username}</p>
                        </div>
                    )}
                    <div className={"form-group col-md-12"}>
                        <label htmlFor="username">Username</label>
                        <Field className={"form-control"} type={"text"} name={"username"} id={"username"}/>
                    </div>

                    {touched.email && errors.email && (
                        <div className={"form-group" + " " + styles.errorContainer}>
                            <p>{errors.email}</p>
                        </div>
                    )}
                    <div className={"form-group col-md-12"}>
                        <label htmlFor="email">Email Address</label>
                        <Field className={"form-control"} type={"email"} name={"email"} id={"email"}/>
                    </div>

                    {touched.password && errors.password && (
                        <div className={"form-group" + " " + styles.errorContainer}>
                            <p>{errors.password}</p>
                        </div>
                    )}
                    <div className={"form-group col-md-12"}>
                        <label htmlFor="password">Password</label>
                        <Field className={"form-control"} type={"password"} name={"password"} id={"password"}/>
                    </div>

                    {touched.confirmedPassword && errors.confirmedPassword && (
                        <div className={"form-group" + " " + styles.errorContainer}>
                            <p>{errors.confirmedPassword}</p>
                        </div>
                    )}
                    <div className={"form-group col-md-12"}>
                        <label htmlFor="confirmedPassword">Confirmed Password</label>
                        <Field className={"form-control"} type={"password"} name={"confirmedPassword"}
                               id={"confirmedPassword"}/>
                    </div>

                    <div className={"form-group"}>
                        <Field disabled={Object.keys(errors).length !== 0 || isSubmitting}
                               className={"btn btn-primary" + " " + styles.submitButton}
                               type={"submit"} name={"signUpBtn"} id={"signUpBtn"} value={"Sign Up"}/>
                    </div>
                </Form>
            </div>
        </div>
    </div>
);

const SignUpFormik = withFormik({
    mapPropsToValues({username, email, password, confirmedPassword, CREATE_CREDENTIAL_WITH_API}) {
        return {
            username: username || "",
            email: email || "",
            password: password || "",
            confirmedPassword: confirmedPassword || "",
            CREATE_CREDENTIAL_WITH_API: CREATE_CREDENTIAL_WITH_API,
            errModal: false,
            signUpErr: ""
        }
    },
    validationSchema: yup.object().shape({
        username: yup.string().required("Username is required"),
        email: yup.string()
            .required("Email address is required")
            .email("Email address is invalid"),
        password: yup.string()
            .required("Password is required")
            .min(8, "Password is too short")
            .matches(/(?=.*?[A-Z])/, {
                message: "At least one uppercase character is required",
                excludeEmptyString: true
            })
            .matches(/(?=.*?[a-z])/, {
                message: "At least one lowercase character is required",
                excludeEmptyString: true
            })
            .matches(/(?=.*?[0-9])/, {message: "At least one number is required", excludeEmptyString: true})
            .matches(/(?=.*?[#?!@$%^&*-])/, {
                message: "At least one special character is required",
                excludeEmptyString: true
            }),
        confirmedPassword: yup.string()
            .required("Confirmed password is required")
            .oneOf([yup.ref("password"), null], "Passwords do not match")
    }),
    handleSubmit({username, email, password, confirmedPassword, errModal, CREATE_CREDENTIAL_WITH_API}, {
        resetForm,
        setSubmitting,
        setFieldValue
    }) {
        CREATE_CREDENTIAL_WITH_API(username, email, password)
            .then((result) => {
                resetForm();
                setSubmitting(false);
            })
            .catch((err) => {
                let error = "";
                if (err.response.data.BadRequestError !== undefined) {
                    error = err.response.data.BadRequestError.shift();
                } else if (err.response.data.ServerError.errors) {
                    error = err.response.data.ServerError.errors.shift().errorMessage;
                }
                setFieldValue("errModal", !errModal);
                setFieldValue("signUpErr", error);
                setSubmitting(false);
            });
    }
})(SignUp)

const mapDispatchToProps = dispatch => ({
    CREATE_CREDENTIAL_WITH_API: (username, email, password) => dispatch(CREATE_CREDENTIAL_WITH_API(username, email, password))
});

export default connect(undefined, mapDispatchToProps)(SignUpFormik);