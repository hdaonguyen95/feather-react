import React from "react";
import {connect} from "react-redux";
import {Field, Form, withFormik} from "formik";
import * as yup from "yup";
import styles from "../../styles/Authentication/SignIn.module.css";
import ErrorModal from "../Modals/ErrorModal";
import {AUTHENTICATE_WITH_API} from "../../actions/Authentication/AuthAction";
import Navigation from "../Navigation/Nagivation";
import feather from "../../images/feather.png";
import {history} from "../../routes/public-route";

const SignIn = ({values, errors, touched, setFieldValue, isSubmitting}) => (
    <div>
        <Navigation/>
        <ErrorModal isOpen={values.errModal} toggle={() => {
            setFieldValue("errModal", !values.errModal);
        }} errorTitle={"Login failed"} errorMessage={values.loginErr}/>
        <div className={styles.container}>
            <div className={styles.headerContainer}>
                <img src={feather} alt="feather"/>
                <h3>Sign In</h3>
            </div>
            <div className={styles.main}>
                <Form>
                    {touched.username && errors.username && (
                        <div className={"form-group" + " " + styles.errorContainer}>
                            <p>{errors.username}</p>
                        </div>
                    )}
                    <div className={"form-group col-md-12 col-sm-12"}>
                        <label htmlFor={"username"}>Username</label>
                        <Field className={"form-control"} type={"text"} name={"username"} id={"username"}/>
                    </div>

                    {touched.password && errors.password && (
                        <div className={"form-group" + " " + styles.errorContainer}>
                            <p>{errors.password}</p>
                        </div>
                    )}
                    <div className={"form-group col-md-12 col-sm-12"}>
                        <label htmlFor={"password"}>Password</label>
                        <Field className={"form-control"} type={"password"} name={"password"} id={"password"}/>
                    </div>

                    <div className={"form-group"}>
                        <Field disabled={Object.keys(errors).length !== 0 || isSubmitting}
                               className={"btn btn-primary" + " " + styles.submitButton}
                               type={"submit"} name={"loginBtn"} id={"loginBtn"} value={"Sign In"}/>
                    </div>
                </Form>
            </div>
        </div>
    </div>
);

const SignInFormik = withFormik({
    mapPropsToValues({username, password, AUTHENTICATE_WITH_API}) {
        return {
            username: username || "",
            password: password || "",
            AUTHENTICATE_WITH_API: AUTHENTICATE_WITH_API,
            errModal: false,
            loginErr: ""
        }
    },
    validationSchema: yup.object().shape({
        username: yup.string().required("Username is required"),
        password: yup.string().required("Password is required")
    }),
    handleSubmit({username, password, errModal, AUTHENTICATE_WITH_API}, {
        resetForm,
        setSubmitting,
        setFieldValue
    }) {
        AUTHENTICATE_WITH_API(username, password)
            .then((result) => {
                resetForm();
                setSubmitting(false);
                history.push("/profile")
            })
            .catch((err) => {
                setFieldValue("errModal", !errModal);
                setFieldValue("loginErr", "Unauthorized incorrect username or password");
                setSubmitting(false);
            });
    }
})(SignIn);

const mapDispatchToProps = dispatch => ({
    AUTHENTICATE_WITH_API: (username, password) => dispatch(AUTHENTICATE_WITH_API(username, password))
});

export default connect(undefined, mapDispatchToProps)(SignInFormik);