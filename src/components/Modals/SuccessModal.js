import React from "react";
import {MDBBtn, MDBContainer, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader} from "mdbreact";

const SuccessModal = (props) => {
    return (
        <MDBContainer>
            <MDBModal isOpen={props.isOpen} toggle={props.toggle}>
                <MDBModalHeader toggle={props.toggle}>{props.successTitle}</MDBModalHeader>
                <MDBModalBody>
                    {props.successMesage}
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="success" onClick={props.toggle}>Close</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        </MDBContainer>
    );
};

export default SuccessModal;