import React from "react";
import {MDBBtn, MDBContainer, MDBModal, MDBModalBody, MDBModalFooter, MDBModalHeader} from "mdbreact";

const ErrorModal = (props) => {
    return (
        <MDBContainer>
            <MDBModal isOpen={props.isOpen} toggle={props.toggle}>
                <MDBModalHeader toggle={props.toggle}>{props.errorTitle}</MDBModalHeader>
                <MDBModalBody>
                    {props.errorMessage}
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="danger" onClick={props.toggle}>Close</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        </MDBContainer>
    );
};

export default ErrorModal;