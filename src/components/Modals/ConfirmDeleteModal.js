import React, {Component} from 'react';
import {MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter, MDBIcon} from
        'mdbreact';
import styles from "../../styles/Modal/Modal.module.css";

const ConfirmDeleteModal = props => (
    <div className={styles.modalContainer}>
        <MDBContainer>
            <MDBModal modalStyle="danger" className="text-white" size="sm" side position="top-right" backdrop={false}
                      isOpen={props.isOpen}
                      toggle={props.toggle}>
                <MDBModalHeader className="text-center" titleClass="w-100" tag="p" toggle={props.toggle}>
                    Are you sure?
                </MDBModalHeader>
                <MDBModalBody className="text-center">
                    <MDBIcon icon="times" size="4x" className="animated rotateIn"/>
                </MDBModalBody>
                <MDBModalFooter className="justify-content-center">
                    <MDBBtn color="danger" onClick={() => {
                        props.toggle();
                        props.remove(props.accessToken, props.id)
                            .catch((err) => {
                                let errMessage = "";
                                const errCode = err.response.status;
                                console.log(err.response);
                                switch (errCode) {
                                    case 401:
                                        errMessage = "Access denied, unauthorized access, please sign in"
                                        break;
                                    case 403:
                                        errMessage = "Access denied, only administrator or manager can perform this operation"
                                        break;
                                    case 500:
                                        errMessage = "The server can not process this request at the moment due to server failure, please try again later"
                                        break;
                                    default:
                                        errMessage = "Unable to remove department record at this moment"
                                        break;
                                }
                                props.setErrMessage(errMessage);
                                props.openErrorModal();
                            })
                    }}>Yes</MDBBtn>
                    <MDBBtn color="danger" outline onClick={props.toggle}>No</MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        </MDBContainer>
    </div>
);

export default ConfirmDeleteModal;