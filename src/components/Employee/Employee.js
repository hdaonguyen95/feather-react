import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Field, Form, withFormik} from "formik";
import * as yup from "yup";
import ErrorModal from "../Modals/ErrorModal";
import Navigation from "../Navigation/Nagivation";
import styles from "../../styles/Employee/Employee.module.css";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import {
    CREATE_EMPLOYEE_API,
    RETRIEVE_EMPLOYEE_BY_IDENTITY_NUMBER_API, RETRIEVE_EMPLOYEE_CONTACT_INFORMATION,
    UPDATE_EMPLOYEE_API
} from "../../actions/Employee/EmployeeAction";
import {
    RETRIEVE_ALL_DEPARTMENT
} from "../../actions/Department/DepartmentAction";
import SuccessModal from "../Modals/SuccessModal";
import Department from "../Department/Department";

const Employee = ({values, errors, touched, setFieldValue, isSubmitting, handleChange, ...props}) => {
    const employeeNumber = props.match.params.employee;
    const [selectedDate, handleDateChange] = useState(new Date());
    const [unlock, setUnlock] = useState(false);
    useEffect(() => {
        props.RETRIEVE_ALL_DEPARTMENT(props.Auth.accessToken)
            .then((result) => {
                setFieldValue("departmentForeignKey", result[0].id);
                if (employeeNumber !== 0 && employeeNumber !== undefined) {
                    props.RETRIEVE_EMPLOYEE_BY_IDENTITY_NUMBER_API(props.Auth.accessToken, employeeNumber)
                        .then((result) => {
                            const employee = result.data;
                            setFieldValue("id", employee.id);
                            setFieldValue("firstName", employee.firstName);
                            setFieldValue("lastName", employee.lastName);
                            setFieldValue("socialSecurityNumber", employee.socialSecurityNumber);
                            setFieldValue("addressStreet", employee.addressStreet);
                            setFieldValue("addressCity", employee.addressCity);
                            setFieldValue("addressState", employee.addressState);
                            setFieldValue("addressPostalCode", employee.addressPostalCode);
                            setFieldValue("isManager", employee.isManager);
                            setFieldValue("managerIdentityNumber", employee.managerIdentityNumber);
                            setFieldValue("employmentStatus", employee.employmentStatus);
                            setFieldValue("hiredDate", new Date(employee.hiredDate));
                            setFieldValue("departmentForeignKey", employee.departmentForeignKey);
                            handleDateChange(new Date(employee.hiredDate));
                        })
                        .catch((err) => {
                            switch (err.response.status) {
                                case 400:
                                    setFieldValue("errorMessage", "The server can not find any employee record, please check employee number and try again");
                                    break;
                                default:
                                    setFieldValue("errorMessage", "The server can not process your request at this moment, please try again");
                                    break;
                            }
                            setFieldValue("errorModal", !values.errorModal);
                        });
                    props.RETRIEVE_EMPLOYEE_CONTACT_INFORMATION(props.Auth.accessToken, employeeNumber)
                        .then((result) => {
                            setFieldValue("emailAddress", result.data.emailAddress);
                        })
                        .catch((err) => {
                            console.log(err.response);
                        });
                }
            })
            .catch((err) => {
                setFieldValue("errorMessage", "The server can not process your request at this moment, please try again");
                setFieldValue("errorModal", !values.errorModal);
            });
    }, []);

    return (
        <div className={styles.eachEmployeeContainer}>
            <Navigation/>
            <ErrorModal isOpen={values.errorModal} toggle={() => {
                setFieldValue("errorModal", !values.errorModal);
            }} errorTitle={"Error"} errorMessage={values.errorMessage}/>

            <SuccessModal isOpen={values.successModal} toggle={() => {
                setFieldValue("successModal", !values.successModal);
            }} successTitle={values.id !== 0 ? "Update successfully" : "Create successfully"}
                          successMesage={values.successMessage}/>
            <div className={styles.container}>
                <div className={styles.main}>
                    <Form>
                        <div className={styles.headerContainer}>
                            <h3>Employee Information</h3>
                            <div hidden={values.id === 0} className={styles.employeeBtnContainer}>
                                <Field onClick={() => {
                                    setUnlock(!unlock);
                                }} className={"btn btn-danger"} type={"button"} name={"unlock"}
                                       value={"Unlock"}/>
                                <a href={values.emailAddress !== ""
                                    ? `mailto:${values.emailAddress}` : null}>
                                    <Field
                                        onClick={() => {
                                            if (values.emailAddress === "") {
                                                setFieldValue("errorMessage", "Can not contact the current employee, the server can not find any email address connected to this employee profile");
                                                setFieldValue("errorModal", !props.errorModal);
                                            }
                                        }}
                                        disabled={Object.keys(errors).length !== 0 || isSubmitting}
                                        className={"btn btn-primary"}
                                        type={"button"} name={"button"} id={"button"} value={"Contact Employee"}/>
                                </a>
                            </div>
                        </div>

                        <div className={"row"}>
                            <div className={"form-group col-md-6 col-sm-12"}>
                                <label htmlFor="firstName">First Name</label>
                                <Field disabled={!unlock && values.id} className={"form-control"} name={"firstName"}
                                       id={"firstName"}
                                       type={"text"}/>
                            </div>

                            <div className={"form-group col-md-6 col-sm-12"}>
                                <label htmlFor="lastName">Last Name</label>
                                <Field disabled={!unlock && values.id} className={"form-control"} name={"lastName"}
                                       id={"lastName"}
                                       type={"text"}/>
                            </div>
                        </div>

                        <div hidden={!unlock && values.id !== 0} className={"row"}>
                            <div className={"form-group col-md-6 col-sm-12"}>
                                <label htmlFor="emailAddress">Email Address</label>
                                <Field disabled={true} className={"form-control"} name={"emailAddress"}
                                       id={"emailAddress"}
                                       type={"email"}/>
                            </div>

                            <div className={"form-group col-md-6 col-sm-12"}>
                                <label htmlFor="socialSecurityNumber">Social Security Number</label>
                                <Field disabled={values.id !== 0} className={"form-control"}
                                       name={"socialSecurityNumber"}
                                       id={"socialSecurityNumber"}
                                       type={"text"}/>
                            </div>
                        </div>

                        <div hidden={!unlock && values.id !== 0} className={"row"}>
                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressStreet && errors.addressStreet && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressStreet}</p>
                                    </div>
                                )}
                                <label htmlFor="addressStreet">Street Number</label>
                                <Field disabled={!unlock && values.id} className={"form-control"} type={"text"}
                                       name={"addressStreet"}
                                       id={"addressStreet"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressCity && errors.addressCity && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressCity}</p>
                                    </div>
                                )}
                                <label htmlFor="addressCity">Address City</label>
                                <Field disabled={!unlock && values.id} className={"form-control"} type={"text"}
                                       name={"addressCity"}
                                       id={"addressCity"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressState && errors.addressState && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressState}</p>
                                    </div>
                                )}
                                <label htmlFor="addressState">Address State</label>
                                <Field disabled={!unlock && values.id} className={"form-control"} type={"text"}
                                       name={"addressState"}
                                       id={"addressState"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressPostalCode && errors.addressPostalCode && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressPostalCode}</p>
                                    </div>
                                )}
                                <label htmlFor="addressPostalCode">Postal Code</label>
                                <Field disabled={!unlock && values.id} className={"form-control"} type={"text"}
                                       name={"addressPostalCode"}
                                       id={"addressPostalCode"}/>
                            </div>
                        </div>

                        <div className={styles.headerContainer}>
                            <h3>Company Information</h3>
                        </div>

                        <div className={"row"}>
                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.isManager && errors.isManager && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.isManager}</p>
                                    </div>
                                )}
                                <label>Manager Status</label>
                                <div className="custom-control custom-checkbox">
                                    <Field disabled={!unlock && values.id} name={"isManager"} type={"checkbox"}
                                           className={"custom-control-input"}
                                           id={"isManager"}/>
                                    <label className="custom-control-label" htmlFor="isManager">I Am Manager</label>
                                </div>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.managerIdentityNumber && errors.managerIdentityNumber && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.managerIdentityNumber}</p>
                                    </div>
                                )}
                                <label htmlFor="managerIdentityNumber">Manager Identity Number</label>
                                <Field value={values.managerIdentityNumber || ""} disabled={!unlock && values.id !== 0}
                                       className={"form-control"} type={"text"}
                                       name={"managerIdentityNumber"}
                                       id={"managerIdentityNumber"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                <label>Employment Status</label>
                                <div className="custom-control custom-radio custom-control-inline">
                                    <input disabled={!unlock && values.id !== 0} checked={values.employmentStatus === ""
                                    || values.employmentStatus === "Full Time"} type="radio" id="fullTime"
                                           name={"employmentStatus"}
                                           className="custom-control-input"
                                           value={"Full Time"} onChange={handleChange}/>
                                    <label className="custom-control-label" htmlFor="fullTime">Full Time</label>
                                </div>
                                <div className="custom-control custom-radio custom-control-inline">
                                    <input checked={values.employmentStatus === "Part Time"}
                                           disabled={!unlock && values.id !== 0}
                                           type="radio" id="partTime" name={"employmentStatus"}
                                           className="custom-control-input"
                                           value={"Part Time"} onChange={handleChange}/>
                                    <label className="custom-control-label" htmlFor="partTime">Part Time</label>
                                </div>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.departmentForeignKey && errors.departmentForeignKey && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.departmentForeignKey}</p>
                                    </div>
                                )}
                                <label htmlFor="departmentForeignKey">Department</label>
                                {values.departmentForeignKey !== 0 ? (
                                    <select disabled={!unlock && values.id !== 0} onChange={handleChange}
                                            name={"departmentForeignKey"}
                                            className={"browser-default custom-select"}
                                            value={values.departmentForeignKey}>
                                        {props.Department.map((department) => (
                                            <option key={department.id}
                                                    value={department.id}>{department.departmentName}</option>
                                        ))}
                                    </select>
                                ) : (
                                    <select disabled={!unlock && values.id !== 0} onChange={handleChange}
                                            name={"departmentForeignKey"}
                                            className={"browser-default custom-select"}>
                                        {props.Department.map((department) => (
                                            <option key={department.id}
                                                    value={department.id}>{department.departmentName}</option>
                                        ))}
                                    </select>
                                )}
                            </div>
                        </div>
                        <div className={"row" + " " + styles.footerGroupContainer}>
                            <div className={"form-group col-md-2 col-sm-12"}>
                                <label htmlFor="hiredDate">Hired Date</label>
                                <MuiPickersUtilsProvider utils={MomentUtils}>
                                    <DatePicker disabled={!unlock && values.id !== 0} value={selectedDate}
                                                onChange={handleDateChange}/>
                                </MuiPickersUtilsProvider>
                                {useEffect(() => {
                                    setFieldValue("hiredDate", new Date(selectedDate));
                                }, [setFieldValue, selectedDate])}
                            </div>
                            <div className={"form-group col-md-3 col-sm-12"}>
                                <Field hidden={!unlock && values.id !== 0} disabled={!unlock && values.id !== 0}
                                       className={"btn btn-warning"} type={"submit"}
                                       name={"unlock"}
                                       value={values.id !== 0 ? "Save Changes" : "Create Employee"}/>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    );
};

const EmployeeFormik = withFormik({
    mapPropsToValues({
                         firstName,
                         lastName,
                         socialSecurityNumber,
                         addressStreet,
                         addressCity,
                         addressState,
                         addressPostalCode,
                         isManager,
                         employmentStatus,
                         hiredDate,
                         managerIdentityNumber,
                         departmentForeignKey,
                         ...props
                     }) {
        return {
            id: 0,
            firstName: firstName || "",
            lastName: lastName || "",
            emailAddress: "",
            socialSecurityNumber: socialSecurityNumber || "",
            addressStreet: addressStreet || "",
            addressCity: addressCity || "",
            addressState: addressState || "",
            addressPostalCode: addressPostalCode || "",
            isManager: isManager || false,
            employmentStatus: employmentStatus || "Full Time",
            hiredDate: hiredDate || "",
            managerIdentityNumber: managerIdentityNumber || "",
            departmentForeignKey: departmentForeignKey || 0,
            errorMessage: "",
            errorModal: false,
            successMessage: "",
            successModal: false,
            ...props
        }
    },
    validationSchema: yup.object().shape({}),
    handleSubmit(props, {
        setSubmitting,
        setFieldValue
    }) {
        if (props.id !== 0 && props.id !== undefined) {
            props.UPDATE_EMPLOYEE_API(props.Auth.accessToken, {
                id: props.id,
                firstName: props.firstName,
                lastName: props.lastName,
                socialSecurityNumber: props.socialSecurityNumber,
                addressStreet: props.addressStreet,
                addressCity: props.addressCity,
                addressState: props.addressState,
                addressPostalCode: props.addressPostalCode,
                isManager: props.isManager,
                employmentStatus: props.employmentStatus,
                hiredDate: new Date(props.hiredDate),
                managerIdentityNumber: props.managerIdentityNumber,
                departmentForeignKey: Number(props.departmentForeignKey)
            })
                .then((result) => {
                    setSubmitting(false);
                    setFieldValue("successMessage", "Employee record has been updated");
                    setFieldValue("successModal", !props.successModal);
                })
                .catch((err) => {
                    setSubmitting(false);
                    setFieldValue("errorMessage", "The server can not update employee record at this time, please try again later");
                    setFieldValue("errorModal", !props.errorModal);
                    console.log(err.response);
                })
        } else {
            props.CREATE_EMPLOYEE_API(props.Auth.accessToken, {
                firstName: props.firstName,
                lastName: props.lastName,
                socialSecurityNumber: props.socialSecurityNumber,
                addressStreet: props.addressStreet,
                addressCity: props.addressCity,
                addressState: props.addressState,
                addressPostalCode: props.addressPostalCode,
                isManager: props.isManager,
                employmentStatus: props.employmentStatus,
                hiredDate: new Date(props.hiredDate),
                managerIdentityNumber: props.managerIdentityNumber ? Number(props.managerIdentityNumber) : null,
                departmentForeignKey: Number(props.departmentForeignKey)
            })
                .then((result) => {
                    setSubmitting(false);
                    setFieldValue("successMessage", "Employee record has been created");
                    setFieldValue("successModal", !props.successModal);
                })
                .catch((err) => {
                    setSubmitting(false);
                    console.log(err.response);
                    if (err.response.data.BadRequestError !== undefined) {
                        setFieldValue("errorMessage", err.response.data.BadRequestError.shift());
                        setFieldValue("errorModal", !props.errorModal);
                    } else {
                        setFieldValue("errorMessage", "The server can not create new employee record at this time, please try again later");
                        setFieldValue("errorModal", !props.errorModal);
                    }
                });
        }
    }
})(Employee);

const mapStateToProps = state => ({
    Auth: state.Auth,
    Department: state.Department
});

const mapDispatchToProps = dispatch => ({
    CREATE_EMPLOYEE_API: (authToken, employee) => dispatch(CREATE_EMPLOYEE_API(authToken, employee)),
    RETRIEVE_EMPLOYEE_BY_IDENTITY_NUMBER_API: (authToken, identityNumber) => dispatch(RETRIEVE_EMPLOYEE_BY_IDENTITY_NUMBER_API(authToken, identityNumber)),
    UPDATE_EMPLOYEE_API: (authToken, updates) => dispatch(UPDATE_EMPLOYEE_API(authToken, updates)),
    RETRIEVE_ALL_DEPARTMENT: (authToken) => dispatch(RETRIEVE_ALL_DEPARTMENT(authToken)),
    RETRIEVE_EMPLOYEE_CONTACT_INFORMATION: (authToken, identityNumber) => dispatch(RETRIEVE_EMPLOYEE_CONTACT_INFORMATION(authToken, identityNumber))
});

export default connect(mapStateToProps, mapDispatchToProps)(EmployeeFormik);