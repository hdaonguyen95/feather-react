import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import Navigation from "../Navigation/Nagivation";
import styles from "../../styles/Employee/Employee.module.css";
import {NavLink} from "react-router-dom";
import {MDBBtn} from "mdbreact";
import {
    RETRIEVE_ALL_EMPLOYEES_BY_DEPARTMENT
} from "../../actions/Employee/EmployeeAction";
import moment from "moment";
import {RETRIEVE_ALL_DEPARTMENT} from "../../actions/Department/DepartmentAction";
import EmployeeFilter from "../Filter/EmployeeFilter";

const AllEmployee = props => {
    const [filteredEmployee, setFilteredEmployee] = useState([]);
    const departmentNumber = props.match.params.department;

    useEffect(() => {
        props.RETRIEVE_ALL_DEPARTMENT(props.Auth.accessToken)
            .then(() => {
                props.RETRIEVE_ALL_EMPLOYEES_BY_DEPARTMENT(props.Auth.accessToken, departmentNumber)
                    .then((result) => {
                        setFilteredEmployee(result);
                    })
                    .catch((err) => {
                        console.log(err)
                    })
            })
    }, []);

    return (
        <div>
            <Navigation/>
            <div className={styles.container}>
                <div className={styles.main}>
                    <form>
                        <div className={"row"}>
                            <div className={"form-group col-md-12"}>
                                <div className={styles.headerContainer}>
                                    <h3>Employees</h3>
                                    <NavLink className={"btn-create-department"} to={"/employee/create"}><MDBBtn color="danger">Create
                                        Employee Profile</MDBBtn></NavLink>
                                </div>
                            </div>
                        </div>
                    </form>
                    <EmployeeFilter setFilteredEmployee={setFilteredEmployee}
                                    Employee={props.Employee}
                                    Department={props.Department}/>
                    <div className={"table-responsive" + " " + styles.employeeTable}>
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">Employee Number</th>
                                <th scope="col">First Name</th>
                                <th scope="col">Last Name</th>
                                <th scope="col">Employment Status</th>
                                <th scope="col">Department</th>
                                <th scope="col">Hired Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            {filteredEmployee.map((employee) => (
                                <tr key={employee.id}>
                                    <td>{employee.id}</td>
                                    <td><NavLink to={`/employee/detail/${employee.id}`}>{employee.firstName}</NavLink>
                                    </td>
                                    <td><NavLink to={`/employee/detail/${employee.id}`}>{employee.lastName}</NavLink>
                                    </td>
                                    <td>{employee.employmentStatus}</td>
                                    <td><NavLink
                                        to={`/department/detail/${employee.departmentForeignKey}`}>{props.Department
                                        .map((department) => {
                                            if (department.id === employee.departmentForeignKey)
                                                return department.departmentName;
                                        })
                                    }</NavLink></td>
                                    <td>{moment(employee.hiredDate).format("MMM/DD/YYYY")}</td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
};

const mapStateToProps = state => ({
    Department: state.Department,
    Employee: state.Employee,
    Auth: state.Auth
});

const mapDispatchToProps = dispatch => ({
    RETRIEVE_ALL_EMPLOYEES_BY_DEPARTMENT: (authToken, department) => dispatch(RETRIEVE_ALL_EMPLOYEES_BY_DEPARTMENT(authToken, department)),
    RETRIEVE_ALL_DEPARTMENT: (authToken) => dispatch(RETRIEVE_ALL_DEPARTMENT(authToken))
});

export default connect(mapStateToProps, mapDispatchToProps)(AllEmployee);