import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import styles from "../../styles/Pictures/Pictures.module.css";
import Navigation from "../Navigation/Nagivation";
import {RETRIEVE_ALL_PICTURES, UPLOAD_PICTURE} from "../../actions/Pictures/PicturesAction";
import jwtDecode from "jwt-decode";
import ErrorModal from "../Modals/ErrorModal";
import SuccessModal from "../Modals/SuccessModal";
import {MDBCol, MDBMask, MDBRow, MDBView} from "mdbreact";

const Pictures = props => {
    const [picture, setPicture] = useState([]);
    const [errModal, setErrModal] = useState(false);
    const [errorMessage, setErrMessage] = useState("");
    const [successModal, setSuccessModal] = useState(false);
    const [pictureGallery, setPictureGallery] = useState([]);

    useEffect(() => {
        props.RETRIEVE_ALL_PICTURES(props.Auth.accessToken, jwtDecode(localStorage.getItem("accessToken")).unique_name, picture.shift())
    }, []);

    useEffect(() => {
        let cnt = 0;
        let pictureContainer = [];
        let pictureRow = [];
        props.Picture.forEach(eachPicture => {
            if (cnt === 3) {
                cnt = 0;
                pictureContainer.push(pictureRow);
                pictureRow = [];
            } else {
                ++cnt;
                pictureRow.push(eachPicture);
            }
        });
        pictureContainer.push(pictureRow);
        setPictureGallery(pictureContainer);
    }, [props.Picture])

    return (
        <div>
            <Navigation/>

            <ErrorModal isOpen={errModal} toggle={() => {
                setErrModal(!errModal);
            }} errorTitle={"Photo Uploading Failed"} errorMessage={errorMessage}/>

            <SuccessModal isOpen={successModal} toggle={() => {
                setSuccessModal(!successModal);
            }} successTitle={"Photo Uploading Successful"}
                          successMesage={"Your photo has been uploaded"}/>

            <div className={styles.container}>
                <div className={styles.main}>
                    <form encType="multipart/form-data" onSubmit={(event) => {
                        event.preventDefault();
                        props.UPLOAD_PICTURE(props.Auth.accessToken, jwtDecode(localStorage.getItem("accessToken")).unique_name, picture.shift())
                            .then((result) => {
                                console.log(result.data)
                                setSuccessModal(!successModal);
                            })
                            .catch((err) => {
                                setErrMessage("Photo uploading process has failed, please try again");
                                setErrModal(!errModal);
                            });
                    }}>
                        <div className={styles.pictureNavContainer}>
                            <h3>Pictures</h3>
                            <button type={"submit"} className={"btn btn-primary"}>Upload Picture</button>
                        </div>
                        <div>
                            <div className="input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text" id="file">
                                      Upload
                                    </span>
                                </div>
                                <div className="custom-file">
                                    <input
                                        onChange={(event => {
                                            setPicture([...event.target.files])
                                        })}
                                        type="file"
                                        className="custom-file-input"
                                        id="file"
                                        aria-describedby="file"
                                    />
                                    <label className="custom-file-label" htmlFor="file">
                                        {picture[0] !== undefined ? picture[0].name : "Choose file"}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div className={styles.pictureCollection}>
                        <MDBRow className="mt-4">
                            {pictureGallery.map(row => (
                                row.map(picture => (
                                    <MDBCol md="4">
                                        <MDBView hover zoom>
                                            <img
                                                src={"data:image/png;base64, " + picture.photoBinary}
                                                className="img-fluid"
                                                alt={picture.name}/>
                                        </MDBView>
                                    </MDBCol>
                                ))
                            ))}
                        </MDBRow>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = state => {
    return ({
        Auth: state.Auth,
        Picture: state.Picture
    });
};

const mapDispatchToProps = dispatch => {
    return ({
        UPLOAD_PICTURE: (authToken, username, body) => dispatch(UPLOAD_PICTURE(authToken, username, body)),
        RETRIEVE_ALL_PICTURES: (authToken, username) => dispatch(RETRIEVE_ALL_PICTURES(authToken, username))
    });
};

export default connect(mapStateToProps, mapDispatchToProps)(Pictures);