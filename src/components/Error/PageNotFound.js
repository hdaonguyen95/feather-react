import React from "react";
import Error from "./Error";

const PageNotFound = props => (
    <div>
        <Error errCode={404} errHeader={"Page Not Found"}
               errMessage={"The page you are looking for might have been removed had its name changed or is temporarily unavailable."}/>
    </div>
);

export default PageNotFound;