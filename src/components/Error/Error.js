import React from "react";
import styles from "../../styles/Error/Error.module.css";

const Error = props => (
    <div className={styles.container}>
        <div id="notfound">
            <div className={styles.notfound}>
                <div className={styles.notfound404}>
                    <div></div>
                    <h1>{props.errCode}</h1>
                </div>
                <h2>{props.errHeader}</h2>
                <p>{props.errMessage}</p>
                <a href="#">home page</a>
            </div>
        </div>
    </div>
);

export default Error;