import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {RETRIEVE_AUTHENTICATION_LOGS} from "../../actions/Authentication/AuthAction";
import styles from "../../styles/Logs/AuthenticationLogs.module.css";
import Navigation from "../Navigation/Nagivation";
import jwtDecode from "jwt-decode";
import moment from "moment";
import ErrorModal from "../Modals/ErrorModal";

const AuthenticationLogs = props => {
    const [errModal, setErrModal] = useState(false);
    const [errorMessage, setErrMessage] = useState("");
    const [authLogs, setAuthLogs] = useState([]);
    useEffect(() => {
        props.RETRIEVE_AUTHENTICATION_LOGS(props.Auth.accessToken, jwtDecode(localStorage.getItem("accessToken")).unique_name)
            .then((result) => {
                setAuthLogs(result.data);
            })
            .catch((err) => {
                setErrMessage("The server can not retrieve your authentication history at this time, please try again");
                setErrModal(!errModal);
            });
    }, []);

    return (
        <div>
            <Navigation/>
            <ErrorModal isOpen={errModal} toggle={() => {
                setErrModal(!errModal);
            }} errorTitle={"Retrieve authentication logs failed"} errorMessage={errorMessage}/>
            <div className={styles.container}>
                <div className={styles.main}>
                    <div className={styles.authLogNavContainer}>
                        <h3>Authentication Logs</h3>
                        {/*<button className={"btn btn-danger"}>Clear</button>*/}
                    </div>
                    <div className={"table-responsive" + " " + styles.authLogTable}>
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">IP Address</th>
                                <th scope="col">Agent</th>
                                <th scope="col">Authentication Time</th>
                            </tr>
                            </thead>
                            <tbody>
                            {authLogs.map(log => {
                                return (
                                    <tr key={log.id}>
                                       <td>{log.ipAddress}</td>
                                        <td>{log.agent}</td>
                                        <td>{moment(log.authenticationTime).format("MMM/DD/YYYY")}</td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

const mapStateToProps = state => ({
    Auth: state.Auth
});

const mapDispatchToProps = dispatch => ({
    RETRIEVE_AUTHENTICATION_LOGS: (authToken, username) => dispatch(RETRIEVE_AUTHENTICATION_LOGS(authToken, username))
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthenticationLogs);

