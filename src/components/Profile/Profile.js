import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Field, Form, withFormik} from "formik";
import * as yup from "yup";
import ErrorModal from "../Modals/ErrorModal";
import Navigation from "../Navigation/Nagivation";
import styles from "../../styles/Profile/Profile.module.css";
import MomentUtils from '@date-io/moment';
import {MuiPickersUtilsProvider, DatePicker} from '@material-ui/pickers';
import jwtDecode from "jwt-decode";
import {
    CREATE_EMPLOYEE_API,
    RETRIEVE_EMPLOYEE_BY_USERNAME,
    UPDATE_EMPLOYEE_API
} from "../../actions/Employee/EmployeeAction";
import {RETRIEVE_ALL_DEPARTMENT} from "../../actions/Department/DepartmentAction";
import {CREATE_CREDENTIAL_PROFILE} from "../../actions/Profile/ProfileAction";
import SuccessModal from "../Modals/SuccessModal";

const Profile = ({values, errors, touched, setFieldValue, isSubmitting, handleChange, ...props}) => {
    const [selectedDate, handleDateChange] = useState(new Date());

    useEffect(() => {
        props.RETRIEVE_ALL_DEPARTMENT(props.Auth.accessToken)
            .then((result) => {
                setFieldValue("departmentForeignKey", result[0].id);
                props.RETRIEVE_EMPLOYEE_BY_USERNAME(props.Auth.accessToken, jwtDecode(localStorage.getItem("accessToken")).unique_name)
                    .then((result) => {
                        const employee = result.data;
                        setFieldValue("id", employee.id);
                        setFieldValue("firstName", employee.firstName);
                        setFieldValue("lastName", employee.lastName);
                        setFieldValue("socialSecurityNumber", employee.socialSecurityNumber);
                        setFieldValue("addressStreet", employee.addressStreet);
                        setFieldValue("addressCity", employee.addressCity);
                        setFieldValue("addressState", employee.addressState);
                        setFieldValue("addressPostalCode", employee.addressPostalCode);
                        setFieldValue("isManager", employee.isManager);
                        setFieldValue("managerIdentityNumber", employee.managerIdentityNumber);
                        setFieldValue("employmentStatus", employee.employmentStatus);
                        setFieldValue("hiredDate", new Date(employee.hiredDate));
                        setFieldValue("departmentForeignKey", employee.departmentForeignKey);
                        handleDateChange(new Date(employee.hiredDate));
                    })
                    .catch((err) => {
                        setFieldValue("errorMessage", "The server can not process your request at this moment, please try again");
                        setFieldValue("errorModal", !values.errorModal);
                    });
            })
            .catch((err) => {
                setFieldValue("errorMessage", "The server can not process your request at this moment, please try again");
                setFieldValue("errorModal", !values.errorModal);
            });
    }, [])

    return (
        <div>
            <Navigation/>

            <ErrorModal isOpen={values.errorModal} toggle={() => {
                setFieldValue("errorModal", !values.errorModal);
            }} errorTitle={"Error"} errorMessage={values.errorMessage}/>

            <SuccessModal isOpen={values.successModal} toggle={() => {
                setFieldValue("successModal", !values.successModal);
            }} successTitle={values.id !== 0 ? "Update successfully" : "Create successfully"}
                          successMesage={values.successMessage}/>

            <div className={styles.container}>
                <div className={styles.main}>
                    <Form>
                        <div className={styles.headerContainer}>
                            <h3>Personal Information</h3>
                            <Field disabled={Object.keys(errors).length !== 0 || isSubmitting}
                                   className={"btn btn-primary"}
                                   type={"submit"} name={"submit"} id={"submit"} value={"Update Profile"}/>
                        </div>
                        <div className={"row"}>
                            <div className={"form-group col-md-4 col-sm-12"}>
                                {touched.firstName && errors.firstName && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.firstName}</p>
                                    </div>
                                )}
                                <label htmlFor="firstName">First Name</label>
                                <Field className={"form-control"} type={"text"} name={"firstName"} id={"firstName"}/>
                            </div>

                            <div className={"form-group col-md-4 col-sm-12"}>
                                {touched.lastName && errors.lastName && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.lastName}</p>
                                    </div>
                                )}
                                <label htmlFor="lastName">Last Name</label>
                                <Field className={"form-control"} type={"text"} name={"lastName"} id={"lastName"}/>
                            </div>

                            <div className={"form-group col-md-4 col-sm-12"}>
                                {touched.socialSecurityNumber && errors.socialSecurityNumber && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.socialSecurityNumber}</p>
                                    </div>
                                )}
                                <label htmlFor="socialSecurityNumber">Social Security Number</label>
                                <Field className={"form-control"} type={"text"} name={"socialSecurityNumber"}
                                       id={"socialSecurityNumber"}/>
                            </div>
                        </div>

                        <div className={"row"}>
                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressStreet && errors.addressStreet && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressStreet}</p>
                                    </div>
                                )}
                                <label htmlFor="addressStreet">Street Number</label>
                                <Field className={"form-control"} type={"text"} name={"addressStreet"}
                                       id={"addressStreet"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressCity && errors.addressCity && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressCity}</p>
                                    </div>
                                )}
                                <label htmlFor="addressCity">Address City</label>
                                <Field className={"form-control"} type={"text"} name={"addressCity"}
                                       id={"addressCity"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressState && errors.addressState && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressState}</p>
                                    </div>
                                )}
                                <label htmlFor="addressState">Address State</label>
                                <Field className={"form-control"} type={"text"} name={"addressState"}
                                       id={"addressState"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.addressPostalCode && errors.addressPostalCode && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.addressPostalCode}</p>
                                    </div>
                                )}
                                <label htmlFor="addressPostalCode">Postal Code</label>
                                <Field className={"form-control"} type={"text"} name={"addressPostalCode"}
                                       id={"addressPostalCode"}/>
                            </div>
                        </div>
                        <div className={styles.headerContainer}>
                            <h3>Company Information</h3>
                        </div>
                        <div className={"row"}>
                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.isManager && errors.isManager && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.isManager}</p>
                                    </div>
                                )}
                                <label>Manager Status</label>
                                <div className="custom-control custom-checkbox">
                                    <Field name={"isManager"} type={"checkbox"}
                                           className={"custom-control-input"}
                                           id={"isManager"}/>
                                    <label className="custom-control-label" htmlFor="isManager">I Am Manager</label>
                                </div>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.managerIdentityNumber && errors.managerIdentityNumber && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.managerIdentityNumber}</p>
                                    </div>
                                )}
                                <label htmlFor="managerIdentityNumber">Manager Identity Number</label>
                                <Field value={values.managerIdentityNumber || ""}
                                       className={"form-control"} type={"text"}
                                       name={"managerIdentityNumber"}
                                       id={"managerIdentityNumber"}/>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                <label>Employment Status</label>
                                <div className="custom-control custom-radio custom-control-inline">
                                    <input checked={values.employmentStatus === ""
                                    || values.employmentStatus === "Full Time"} type="radio" id="fullTime"
                                           name={"employmentStatus"}
                                           className="custom-control-input"
                                           value={"Full Time"} onChange={handleChange}/>
                                    <label className="custom-control-label" htmlFor="fullTime">Full Time</label>
                                </div>
                                <div className="custom-control custom-radio custom-control-inline">
                                    <input checked={values.employmentStatus === "Part Time"}
                                           type="radio" id="partTime" name={"employmentStatus"}
                                           className="custom-control-input"
                                           value={"Part Time"} onChange={handleChange}/>
                                    <label className="custom-control-label" htmlFor="partTime">Part Time</label>
                                </div>
                            </div>

                            <div className={"form-group col-md-3 col-sm-12"}>
                                {touched.departmentForeignKey && errors.departmentForeignKey && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.departmentForeignKey}</p>
                                    </div>
                                )}
                                <label htmlFor="departmentForeignKey">Department</label>
                                {values.departmentForeignKey !== 0 ? (
                                    <select onChange={handleChange}
                                            name={"departmentForeignKey"}
                                            className={"browser-default custom-select"}
                                            value={values.departmentForeignKey}>
                                        {props.Department.map((department) => (
                                            <option key={department.id}
                                                    value={department.id}>{department.departmentName}</option>
                                        ))}
                                    </select>
                                ) : (
                                    <select onChange={handleChange}
                                            name={"departmentForeignKey"}
                                            className={"browser-default custom-select"}>
                                        {props.Department.map((department) => (
                                            <option key={department.id}
                                                    value={department.id}>{department.departmentName}</option>
                                        ))}
                                    </select>
                                )}
                            </div>
                        </div>
                        <div className={"row"}>
                            <div className={"form-group col-md-2 col-sm-12"}>
                                <label htmlFor="hiredDate">Hired Date</label>
                                <MuiPickersUtilsProvider utils={MomentUtils}>
                                    <DatePicker value={selectedDate}
                                                onChange={handleDateChange}/>
                                </MuiPickersUtilsProvider>
                                {useEffect(() => {
                                    setFieldValue("hiredDate", new Date(selectedDate));
                                }, [setFieldValue, selectedDate])}
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    );
};

const ProfileFormik = withFormik({
        mapPropsToValues({
                             firstName,
                             lastName,
                             socialSecurityNumber,
                             addressStreet,
                             addressCity,
                             addressState,
                             addressPostalCode,
                             isManager,
                             employmentStatus,
                             hiredDate,
                             managerIdentityNumber,
                             departmentForeignKey,
                             ...props
                         }) {
            return {
                id: 0,
                firstName: firstName || "",
                lastName: lastName || "",
                emailAddress: "",
                socialSecurityNumber: socialSecurityNumber || "",
                addressStreet: addressStreet || "",
                addressCity: addressCity || "",
                addressState: addressState || "",
                addressPostalCode: addressPostalCode || "",
                isManager: isManager || false,
                employmentStatus: employmentStatus || "Full Time",
                hiredDate: hiredDate || "",
                managerIdentityNumber: managerIdentityNumber || "",
                departmentForeignKey: departmentForeignKey || 0,
                errorMessage: "",
                errorModal: false,
                successMessage: "",
                successModal: false,
                ...props
            }
        },
        validationSchema: yup.object().shape({
            firstName: yup.string().required("First name is required"),
            lastName: yup.string().required("Last name is required"),
            socialSecurityNumber: yup.string()
                .required("Social security number is required")
                .matches(/^[0-9]+$/, {message: "Social security number is invalid, must contain only number"})
                .min(9, "Social security number must be nine characters")
                .max(9, "Social security number must be nine characters"),
            addressStreet: yup.string()
                .required("Address street is required")
                .matches(/^\d{1,5}\s.*\w+$/, {message: "Street number is invalid"}),
            addressCity: yup.string()
                .required("Address city is required")
                .matches(/^[a-zA-Z ]+$/, {message: "Address city is invalid, must contain only letters"}),
            addressState: yup.string()
                .required("Address state is required")
                .matches(/^[a-zA-Z ]+$/, {message: "Address state is invalid, must contain only letters"}),
            addressPostalCode: yup.string()
                .required("Postal code is required")
                .matches(/^[a-zA-Z][0-9][a-zA-Z][0-9][a-zA-Z][0-9]$/, {message: "Postal code is invalid"})
                .min(6, "Postal code must be six characters")
                .max(6, "Postal code must be six characters"),
        }),
        handleSubmit(props, {
            resetForm,
            setSubmitting,
            setFieldValue
        }) {
            setSubmitting(false);
            if (props.id !== 0 && props.id !== undefined) {
                props.UPDATE_EMPLOYEE_API(props.Auth.accessToken, {
                    id: props.id,
                    firstName: props.firstName,
                    lastName: props.lastName,
                    socialSecurityNumber: props.socialSecurityNumber,
                    addressStreet: props.addressStreet,
                    addressCity: props.addressCity,
                    addressState: props.addressState,
                    addressPostalCode: props.addressPostalCode,
                    isManager: props.isManager,
                    employmentStatus: props.employmentStatus,
                    hiredDate: new Date(props.hiredDate),
                    managerIdentityNumber: props.managerIdentityNumber,
                    departmentForeignKey: Number(props.departmentForeignKey)
                })
                    .then((result) => {
                        setSubmitting(false);
                        setFieldValue("successMessage", "Your profile has been updated");
                        setFieldValue("successModal", !props.successModal);
                    })
                    .catch((err) => {
                        setSubmitting(false);
                        setFieldValue("errorMessage", "The server can not update your profile at this time, please try again later");
                        setFieldValue("errorModal", !props.errorModal);
                        console.log(err.response);
                    })
            } else {
                props.CREATE_EMPLOYEE_API(props.Auth.accessToken, {
                    firstName: props.firstName,
                    lastName: props.lastName,
                    socialSecurityNumber: props.socialSecurityNumber,
                    addressStreet: props.addressStreet,
                    addressCity: props.addressCity,
                    addressState: props.addressState,
                    addressPostalCode: props.addressPostalCode,
                    isManager: props.isManager,
                    employmentStatus: props.employmentStatus,
                    hiredDate: new Date(props.hiredDate),
                    managerIdentityNumber: props.managerIdentityNumber ? Number(props.managerIdentityNumber) : null,
                    departmentForeignKey: Number(props.departmentForeignKey)
                })
                    .then((result) => {
                        setSubmitting(false);
                        const username = jwtDecode(props.Auth.accessToken).unique_name;
                        props.CREATE_CREDENTIAL_PROFILE(props.Auth.accessToken, username, result.data.id)
                            .then((result) => {
                                setFieldValue("successMessage", "Your profile has been created");
                                setFieldValue("successModal", !props.successModal);
                            })
                            .catch((err) => {
                                setFieldValue("errorMessage", "The server can not create your profile record at this time, please try again later");
                                setFieldValue("errorModal", !props.errorModal);
                            });
                    })
                    .catch((err) => {
                        setSubmitting(false);
                        if (err.response.data.BadRequestError !== undefined) {
                            setFieldValue("errorMessage", err.response.data.BadRequestError.shift());
                            setFieldValue("errorModal", !props.errorModal);
                        } else {
                            setFieldValue("errorMessage", "The server can not create your profile record at this time, please try again later");
                            setFieldValue("errorModal", !props.errorModal);
                        }
                    });
            }
        }
    }
)(Profile);

const mapStateToProps = state => ({
    Auth: state.Auth,
    Department: state.Department
});

const mapDispatchToProps = dispatch => (
    {
        CREATE_EMPLOYEE_API: (authToken, employee) => dispatch(CREATE_EMPLOYEE_API(authToken, employee)),
        UPDATE_EMPLOYEE_API: (authToken, updates) => dispatch(UPDATE_EMPLOYEE_API(authToken, updates)),
        RETRIEVE_ALL_DEPARTMENT: (authToken) => dispatch(RETRIEVE_ALL_DEPARTMENT(authToken)),
        RETRIEVE_EMPLOYEE_BY_USERNAME: (authToken, username) => dispatch(RETRIEVE_EMPLOYEE_BY_USERNAME(authToken, username)),
        CREATE_CREDENTIAL_PROFILE: (authToken, username, profileId) => dispatch(CREATE_CREDENTIAL_PROFILE(authToken, username, profileId))
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(ProfileFormik);