import React from "react";
import Navigation from "../Navigation/Nagivation";
import styles from "../../styles/About/About.module.css";

const About = props => (
    <div>
        <Navigation/>
        <div className={styles.container}>
            <div className={styles.main}>
                <h4>Welcome to Feather.</h4>
                <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                        <h4 className="display-4">Introduction</h4>
                        <p className="lead">Feather is one of my web development projects at college that I created to
                            simulate an employee management system.
                            In the first design which is done by the school, the language of choice for the backend
                            system is Node.JS and we use this language for web development because it is easy to use and
                            there are not a lot of concepts that we need to know to build a system that contains both
                            backend and frontend. However, because I want to learn more about backend development and I
                            am more interested in DotNet, C# as well as React.JS so I decided to learn everything on my
                            own and apply what I know to develop this system because the best way of learning something
                            is building something with it.</p>
                    </div>
                </div>

                <div className="jumbotron jumbotron-fluid">
                    <div className="container">
                        <h4 className="display-4">System Development</h4>
                        <p className="lead">This project is an upgrade of the prototype that I built using Node.JS and
                            in general this system using the SPA architecture.
                            All the backend of the application is written in C# with the DotNet framework while all the
                            frontend is written in JavaScript.
                            Because this is a complex application and since it has a lot of modules, I need to use the
                            repository design pattern to make the code easier to maintain. So all the business logic of
                            the system such as how the backend handles the request internally is done by using separate
                            modules instead of writing all functions into the controller like the first design that I
                            created with Node.JS. Apply the repository design, each module will need to implement an
                            interface and to use them we need to inject them into the DI controller which is defined in
                            the StartUp class of DotNet and each controller will register the modules that it needs in
                            the constructor. Next, the controller will receive the requests of the client that comes
                            from the React application and it will need to call the function for the job which is
                            provided by the modules using the function signature declared inside the interface injected
                            in the constructor.
                            The backend system uses SQL server for deployment however I used SQLite while developing
                            this application. For DotNet, I use the Entity framework to manage the database so it is
                            really easy to manage the database and I do not need to write any SQL scripts to manage the
                            database. However, when I populate the database I need to use the Faker library to generate
                            the data of the system for demonstration and I need to add all SQL command that I generate
                            using JavaScript, paste them into the IDE for error checking and then populate the database.
                            To protect the route in the API, I used JWT for authorization and this process only requires
                            the user to enter their credential only one time. If the credential is correct then they
                            will have the token which is used to access other resources of the system. There is also
                            role-based authorization but I did not write any front-end function to use this one even
                            though I have this implementation in the backend because this project is quite complex and I
                            want to focus on more important stuff so I skip this one. Nonetheless, the role-based
                            authentication can be used to check if the user has the right to remove the department or
                            not. This is the only implementation that I created with the role-based authorization for
                            the frontend but I just did not create an interface for CRUD operations for the role-based
                            authorization. This is the first SPA application that I implement for both backend and
                            frontend. There are still a lot of bugs and if you look into the codebase, it is not good
                            and I could have done better by applying the clean design to keep the controller code to be
                            much more cleaner and less dependent on each other module. But I am glad that I know a bit
                            of how to make the front end and the backend work together and the process of starting to
                            learn all these technologies in three months is amazing. In the next project to come, I
                            think I will learn more about design patterns, applying the clean design and develop each
                            module with the unit testing as well so the code can be easier to maintain and upgrade.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default About;