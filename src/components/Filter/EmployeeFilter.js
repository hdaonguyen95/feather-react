import React, {useState, useEffect} from "react";
import {DatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import styles from "../../styles/Filter/EmployeeFilter.module.css";
import {
    FILTER_EMPLOYEE_BY_DEPARTMENT, FILTER_EMPLOYEE_BY_EMPLOYMENT_STATUS,
    FILTER_EMPLOYEE_BY_FIRST_NAME, FILTER_EMPLOYEE_BY_HIRED_DATE,
    FILTER_EMPLOYEE_BY_LAST_NAME
} from "../../actions/Employee/EmployeeAction";

const retrieveEmployeeDataWithFilter = (filter, employeeData) => {
    if (filter.filter === true && filter.filterBy !== "") {
        if (filter.filterBy === "firstName")
            return FILTER_EMPLOYEE_BY_FIRST_NAME(filter.filterEmployeeName, employeeData);
        else if (filter.filterBy === "lastName")
            return FILTER_EMPLOYEE_BY_LAST_NAME(filter.filterEmployeeName, employeeData);
        else if (filter.filterBy === "department")
            return FILTER_EMPLOYEE_BY_DEPARTMENT(filter.filterDepartment, employeeData);
        else if (filter.filterBy === "employmentStatus")
            return FILTER_EMPLOYEE_BY_EMPLOYMENT_STATUS(filter.filterEmploymentStatus, employeeData);
        else if (filter.filterBy === "hiredDate")
            return FILTER_EMPLOYEE_BY_HIRED_DATE(filter.filterStartDate, filter.filterEndDate, employeeData);
        else
            return employeeData;
    } else {
        return employeeData;
    }
};

const EmployeeFilter = props => {
    const [filter, setFilter] = useState(false);
    const [filterBy, setFilterBy] = useState("");
    const [filterEmployeeName, setFilterEmployeeName] = useState("");
    const [filterEmploymentStatus, setFilterEmploymentStatus] = useState("");
    const [filterDepartment, setFilterDepartment] = useState("");
    const [filterStartDate, setFilterStartDate] = useState(new Date());
    const [filterEndDate, setFilterEndDate] = useState(new Date());

    useEffect(() => {
        props.setFilteredEmployee(retrieveEmployeeDataWithFilter({
            filter,
            filterBy,
            filterEmployeeName,
            filterEmploymentStatus,
            filterDepartment,
            filterStartDate,
            filterEndDate
        }, props.Employee));
    }, [
        filter,
        filterBy,
        filterEmployeeName,
        filterEmploymentStatus,
        filterDepartment,
        filterStartDate,
        filterEndDate
    ]);

    return (
        <div className={styles.container}>
            <div className={"row"}>
                <div className={"form-group col-md-12 col-sm-12" + " " + styles.filterBtnContainer}>
                    <input onClick={() => {
                        setFilter(!filter);
                    }} value={"Enable Filter"} className={"btn btn-primary"} type={"button"}
                           id={"enableFilter"}/>
                </div>
            </div>
            <div className={"row"}>
                <div className={"form-group col-md-12 col-sm-12"}>
                    <label htmlFor="departmentForeignKey">Filter By</label>
                    <select disabled={!filter} onChange={(event => {
                        setFilterBy(event.target.value);
                    })}
                            name={"filterBy"}
                            className={"browser-default custom-select"}>
                        <option value="">Choose Filter</option>
                        <option value={"firstName"}>First Name</option>
                        <option value={"lastName"}>Last Name</option>
                        <option value={"employmentStatus"}>Employment Status</option>
                        <option value={"department"}>Department</option>
                        <option value={"hiredDate"}>Hired Date</option>
                    </select>
                </div>
            </div>
            <div className={"row"}>
                <div className={"form-group col-md-6 col-sm-12"}>
                    <label htmlFor="filter">Employee Name</label>
                    <input disabled={!filter || (filterBy !== "firstName" && filterBy !== "lastName")}
                           onChange={(event => {
                               setFilterEmployeeName(event.target.value);
                           })}
                           className={"form-control"}
                           type={"text"}
                           id={"filter"}/>
                </div>

                <div className={"form-group col-md-6 col-sm-12"}>
                    <label htmlFor="departmentForeignKey">Department</label>
                    <select disabled={!filter || filterBy !== "department"}
                            onChange={(event => {
                                setFilterDepartment(event.target.value);
                            })}
                            className={"browser-default custom-select"}>
                        {props.Department.map((department) => (
                            <option key={department.id}
                                    value={department.id}>{department.departmentName}</option>
                        ))}
                    </select>
                </div>
            </div>

            <div className={"row"}>
                <div className={"form-group col-md-4 col-sm-12"}>
                    <label>Employment Status</label>
                    <div className="custom-control custom-radio custom-control-inline">
                        <input name={"employmentStatus"} disabled={!filter || filterBy !== "employmentStatus"}
                               type="radio" id="fullTime"
                               className="custom-control-input"
                               value={"Full Time"} onChange={(event => {
                            setFilterEmploymentStatus(event.target.value);
                        })}/>
                        <label className="custom-control-label" htmlFor="fullTime">Full Time</label>
                    </div>
                    <div className="custom-control custom-radio custom-control-inline">
                        <input name={"employmentStatus"} disabled={!filter || filterBy !== "employmentStatus"}
                               type="radio" id="partTime"
                               className="custom-control-input"
                               value={"Part Time"} onChange={(event => {
                            setFilterEmploymentStatus(event.target.value);
                        })}/>
                        <label className="custom-control-label" htmlFor="partTime">Part Time</label>
                    </div>
                </div>
                <div className={"form-group col-md-4 col-sm-12"}>
                    <label>Start Date</label>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <DatePicker disabled={!filter || filterBy !== "hiredDate"} value={filterStartDate}
                                    onChange={setFilterStartDate}/>
                    </MuiPickersUtilsProvider>
                </div>

                <div className={"form-group col-md-4 col-sm-12"}>
                    <label>End Date</label>
                    <MuiPickersUtilsProvider utils={MomentUtils}>
                        <DatePicker disabled={!filter || filterBy !== "hiredDate"} value={filterEndDate}
                                    onChange={setFilterEndDate}/>
                    </MuiPickersUtilsProvider>
                </div>
            </div>
        </div>
    );
};

export default EmployeeFilter;