import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import styles from "../../styles/SideBar/SideBar.module.css";
import {connect} from "react-redux";
import {SIGNOUT} from "../../actions/Authentication/AuthAction";

const SideBar = props => {
    return (
        <div className={styles.container + " " + props.openSideBar}>
            <div className={styles.main}>
                {!!props.Auth.accessToken ? (
                    <div>
                        <NavLink to={"/employee/"}>Employees</NavLink>
                        <NavLink to={"/employee/manager"}>Manager</NavLink>
                        <NavLink to={"/department/"}>Departments</NavLink>
                        <NavLink to={"/authentication/authLogs"}>Authentication Logs</NavLink>
                        <NavLink to={"/profile"}>Profile</NavLink>
                    </div>
                ) : null}
                {!!!props.Auth.accessToken ?
                    (<div>
                        <NavLink to={"/auth/sign-up"}>Sign Up</NavLink>
                        <NavLink to={"/auth/sign-in"}>Sign In</NavLink>
                    </div>)
                    :
                    (<div>
                        <a onClick={() => {props.SIGNOUT()}} href="/auth/sign-in">Sign Out</a>
                    </div>)}
            </div>
        </div>
    )
};

const mapStateToProps = state => ({
    Auth: state.Auth
});

const mapDispatchToProps = dispatch => ({
    SIGNOUT: () => dispatch(SIGNOUT())
});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);