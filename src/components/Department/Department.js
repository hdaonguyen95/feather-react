import React, {useEffect} from "react";
import {connect} from "react-redux";
import {Field, Form, withFormik} from "formik";
import * as yup from "yup";
import ErrorModal from "../Modals/ErrorModal";
import Navigation from "../Navigation/Nagivation";
import styles from "../../styles/Department/Department.module.css";
import {NavLink} from "react-router-dom";
import {MDBBtn} from "mdbreact";
import {
    CREATE_DEPARTMENT,
    GENERATE_DEPARTMENT_REPORT,
    RETRIEVE_DEPARTMENT_BY_NUMBER,
    UPDATE_DEPARTMENT_WITH_API
} from "../../actions/Department/DepartmentAction";
import SuccessModal from "../Modals/SuccessModal";

const Department = ({
                        values,
                        errors,
                        touched,
                        setFieldValue,
                        isSubmitting,
                        RETRIEVE_DEPARTMENT_BY_NUMBER,
                        GENERATE_DEPARTMENT_REPORT,
                        Auth,
                        match,
                    }) => {
    let departmentIdentityNumber = match.params.departmentNumber;
    useEffect(() => {
        if (departmentIdentityNumber !== 0 && departmentIdentityNumber !== undefined) {
            RETRIEVE_DEPARTMENT_BY_NUMBER(Auth.accessToken, departmentIdentityNumber)
                .then((department) => {
                    setFieldValue("id", department.id);
                    setFieldValue("departmentName", department.departmentName);
                })
                .catch((err) => {
                    setFieldValue("id", 0);
                    setFieldValue("departmentName", "");
                    switch (err.response.status) {
                        case 404:
                            setFieldValue("errorMessage", "The server can not find any department record, please check department number and try again");
                            break;
                        case 401:
                            setFieldValue("errorMessage", "Access denied, unauthorized access, please sign in");
                            break;
                        default:
                            setFieldValue("errorMessage", "The server can not process your request at this moment, please try again");
                            break;
                    }
                    setFieldValue("errModal", !values.errModal);
                });
            GENERATE_DEPARTMENT_REPORT(Auth.accessToken, departmentIdentityNumber)
                .then((result) => {
                    setFieldValue("report", result.data);
                })
                .catch((err) => {
                    setFieldValue("id", 0);
                    setFieldValue("report", {});
                    switch (err.response.status) {
                        case 404:
                            setFieldValue("errorMessage", "The server can not find any department record, please check department number and try again");
                            break;
                        case 401:
                            setFieldValue("errorMessage", "Access denied, unauthorized access, please sign in");
                            break;
                        default:
                            setFieldValue("errorMessage", "The server can not process your request at this moment, please try again");
                            break;
                    }
                    setFieldValue("errModal", !values.errModal);
                });
        }
    }, []);

    return (
        <div>
            <Navigation/>
            <ErrorModal isOpen={values.errModal} toggle={() => {
                setFieldValue("errModal", !values.errModal);
            }} errorTitle={"Update failed"} errorMessage={values.errorMessage}/>

            <SuccessModal isOpen={values.successModal} toggle={() => {
                setFieldValue("successModal", !values.successModal);
            }} successTitle={departmentIdentityNumber !== 0 ? "Update successfully" : "Create successfully"}
                          successMesage={values.successMessage}/>
            <div className={styles.container}>
                <div className={styles.main}>
                    <Form>
                        <div className={styles.headerContainer}>
                            <h3>Department Information</h3>
                            {departmentIdentityNumber !== undefined ? (
                                <Field
                                    disabled={Object.keys(errors).length !== 0 || isSubmitting || values.id == 0}
                                    className={"btn btn-primary"}
                                    type={"submit"} name={"submit"} id={"submit"} value={"Update Department"}/>
                            ) : (
                                <Field disabled={Object.keys(errors).length !== 0 || isSubmitting}
                                       className={"btn btn-success"}
                                       type={"submit"} name={"submit"} id={"submit"} value={"Create Department"}/>
                            )}
                        </div>

                        <div className={"row"}>
                            <div className={"form-group col-md-12 col-sm-12"}>
                                {touched.departmentName && errors.departmentName && (
                                    <div className={styles.errorContainer}>
                                        <p>{errors.departmentName}</p>
                                    </div>
                                )}
                                <label htmlFor="departmentName">Department Name</label>
                                <Field value={values.departmentName || ""} className={"form-control"} type={"text"}
                                       name={"departmentName"}
                                       id={"departmentName"}/>
                            </div>
                        </div>

                        <div hidden={values.id === 0} className={"row"}>
                            <div className={"form-group col-md-12 col-sm-12"}>
                                <h4>All People In Department: {values.report.all}</h4>
                                <NavLink to={`/employee/department/${values.id}`}><MDBBtn color="warning">View All
                                    Employee(s)</MDBBtn></NavLink>
                            </div>

                            <div className={"form-group col-md-12 col-sm-12"}>
                                <h4>Number Of Manager(s): {values.report.manager}</h4>
                                <NavLink to={`/employee/manager/department/${values.id}`}><MDBBtn color="danger">View
                                    Manager(s)</MDBBtn></NavLink>
                            </div>

                            <div className={"form-group col-md-12 col-sm-12"}>
                                <h4>Number Of Employee(s): {values.report.employee}</h4>
                                <NavLink to={`/employee/normal/department/${values.id}`}><MDBBtn color="success">View
                                    Employee(s)</MDBBtn></NavLink>
                            </div>
                        </div>
                    </Form>
                </div>
            </div>
        </div>
    );
};

const DepartmentFormik = withFormik({
    mapPropsToValues({
                         departmentName,
                         Auth,
                         UPDATE_DEPARTMENT_WITH_API,
                         CREATE_DEPARTMENT
                     }) {
        return {
            id: 0,
            departmentName: departmentName || "",
            Auth: Auth,
            UPDATE_DEPARTMENT_WITH_API: UPDATE_DEPARTMENT_WITH_API,
            errModal: false,
            successModal: false,
            errorMessage: "",
            successMessage: "",
            report: {},
            CREATE_DEPARTMENT: CREATE_DEPARTMENT
        }
    },
    validationSchema: yup.object().shape({
        departmentName: yup.string()
            .required("Department name is required")
            .matches(/^[a-zA-Z ]+$/, {message: "Department name is invalid, must contain only letters"})
    }),
    handleSubmit({
                     id,
                     departmentName,
                     Auth,
                     UPDATE_DEPARTMENT_WITH_API,
                     CREATE_DEPARTMENT,
                     errModal,
                     successModal
                 }, {
                     setSubmitting,
                     setFieldValue
                 }) {
        if (id === 0) {
            CREATE_DEPARTMENT(Auth.accessToken, {id, departmentName})
                .then(() => {
                    setSubmitting(false);
                    setFieldValue("successMessage", "New department has been created");
                    setFieldValue("successModal", !successModal);
                })
                .catch((err) => {
                    setSubmitting(false);
                    switch (err.response.status) {
                        case 401:
                            setFieldValue("errorMessage", "Access denied, unauthorized access, please sign in");
                            break;
                        case 403:
                            setFieldValue("errorMessage", "Access denied, only administrator or manager can perform this operation");
                            break;
                        case 404:
                            setFieldValue("errorMessage", "The server can not find any department record, please check department number and try again");
                            break;
                        case 500:
                            setFieldValue("errorMessage", "The server can not process this request at the moment due to server failure, please try again later");
                            break;
                        default:
                            setFieldValue("errorMessage", "The server can not create new department record at this time, please try again later");
                            break;
                    }
                    setFieldValue("errModal", !errModal);
                });
        } else {
            UPDATE_DEPARTMENT_WITH_API(Auth.accessToken, {id, departmentName})
                .then(() => {
                    setSubmitting(false);
                    setFieldValue("successMessage", "Department name has been updated");
                    setFieldValue("successModal", !successModal);
                })
                .catch((err) => {
                    setSubmitting(false);
                    switch (err.response.status) {
                        case 401:
                            setFieldValue("errorMessage", "Access denied, unauthorized access, please sign in");
                            break;
                        case 403:
                            setFieldValue("errorMessage", "Access denied, only administrator or manager can perform this operation");
                            break;
                        case 404:
                            setFieldValue("errorMessage", "The server can not find any department record, please check department number and try again");
                            break;
                        case 500:
                            setFieldValue("errorMessage", "The server can not process this request at the moment due to server failure, please try again later");
                            break;
                        default:
                            setFieldValue("errorMessage", "The server can not update department record at this time, please try again later");
                            break;
                    }
                    setFieldValue("errModal", !errModal);
                });
        }
    }
})(Department);

const mapStateToProps = state => ({
    Auth: state.Auth,
});

const mapDispatchToProps = dispatch => ({
        RETRIEVE_DEPARTMENT_BY_NUMBER: (authToken, departmentNumber) => dispatch(RETRIEVE_DEPARTMENT_BY_NUMBER(authToken, departmentNumber)),
        UPDATE_DEPARTMENT_WITH_API: (authToken, updates) => dispatch(UPDATE_DEPARTMENT_WITH_API(authToken, updates)),
        GENERATE_DEPARTMENT_REPORT: (authToken, departmentNumber) => dispatch(GENERATE_DEPARTMENT_REPORT(authToken, departmentNumber)),
        CREATE_DEPARTMENT: (authToken, department) => dispatch(CREATE_DEPARTMENT(authToken, department))
    }
);

export default connect(mapStateToProps, mapDispatchToProps)(DepartmentFormik);