import React, {useEffect, useState} from "react";
import {connect} from "react-redux";
import {Field, Form, withFormik} from "formik";
import * as yup from "yup";
import ErrorModal from "../Modals/ErrorModal";
import Navigation from "../Navigation/Nagivation";
import styles from "../../styles/Department/Department.module.css";
import {NavLink} from "react-router-dom";
import {MDBBtn} from "mdbreact";
import {
    FILTER_DEPARTMENT_BY_NAME,
    REMOVE_DEPARTMENT_WITH_API,
    RETRIEVE_ALL_DEPARTMENT
} from "../../actions/Department/DepartmentAction";
import ConfirmDeleteModal from "../Modals/ConfirmDeleteModal";

const AllDepartment = ({
                           RETRIEVE_ALL_DEPARTMENT,
                           REMOVE_DEPARTMENT_WITH_API,
                           FILTER_DEPARTMENT_BY_NAME,
                           Departments,
                           Auth,
                           values
                       }) => {
    useEffect(() => {
        RETRIEVE_ALL_DEPARTMENT(Auth.accessToken)
            .then((departments) => {
                setFilteredDepartments(departments);
            });
    }, []);

    useEffect(() => {
        if (values.filter !== "") {
            const result = FILTER_DEPARTMENT_BY_NAME(values.filter, Departments);
            setFilteredDepartments(result);
        } else {
            setFilteredDepartments(Departments);
        }
    }, [values.filter]);

    const [deleteIdentityNumber, setDeleteIdentityNumber] = useState(0);
    const [activeDeleteModal, setActiveDeleteModal] = useState(false);
    const [errModal, setErrModal] = useState(false);
    const [errorMessage, setErrMessage] = useState("");
    const [filteredDepartments, setFilteredDepartments] = useState([]);

    return (
        <div>
            <Navigation/>
            <ErrorModal isOpen={errModal} toggle={() => {
                setErrModal(!errModal);
            }} errorTitle={"Remove department failed"} errorMessage={errorMessage}/>

            <ConfirmDeleteModal isOpen={activeDeleteModal}
                                accessToken={Auth.accessToken}
                                id={deleteIdentityNumber}
                                remove={REMOVE_DEPARTMENT_WITH_API}
                                setErrMessage={setErrMessage}
                                openErrorModal={() => {
                                    setErrModal(!errModal)
                                }}
                                toggle={() => {
                                    setActiveDeleteModal(!activeDeleteModal);
                                }}/>
            <div className={styles.container}>
                <div className={styles.main}>
                    <Form>
                        <div className={"row"}>
                            <div className={"form-group col-md-12"}>
                                <div className={styles.headerContainer}>
                                    <h3>Departments</h3>
                                    <NavLink className={"btn-create-department"}
                                             to={"/department/create"}><MDBBtn color="success">Create
                                        Department</MDBBtn></NavLink>
                                </div>
                            </div>
                        </div>

                        <div className={"row"}>
                            <div className={"form-group col-md-6 col-sm-12"}>
                                <label htmlFor="filter">Find Department</label>
                                <Field className={"form-control"} type={"text"} name={"filter"} id={"filter"}/>
                            </div>
                        </div>
                    </Form>
                    <div className={"table-responsive" + " " + styles.departmentTable}>
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col">Department Number</th>
                                <th scope="col">Department Name</th>
                                <th scope="col">Remove Department</th>
                            </tr>
                            </thead>
                            <tbody>
                            {filteredDepartments.map((department) => {
                                return (
                                    <tr key={department.id}>
                                        <td>{department.id}</td>
                                        <td><NavLink
                                            to={`/department/detail/${department.id}`}>{department.departmentName}</NavLink>
                                        </td>
                                        <td><MDBBtn
                                            onClick={() => {
                                                setDeleteIdentityNumber(department.id);
                                                setActiveDeleteModal(!activeDeleteModal);
                                            }}
                                            color="danger">Remove</MDBBtn></td>
                                    </tr>
                                );
                            })}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

const AllDepartmentFormik = withFormik({
    mapPropsToValues({filter}) {
        return {
            filter: filter || "",
        }
    },
    validationSchema: yup.object().shape({
        filter: yup.string()
            .matches(/^[a-zA-Z ]+$/, {message: "Filter is invalid, must contain only letters"})
    }),
    handleSubmit({filter}, {
        resetForm,
        setSubmitting,
        setFieldValue,
    }) {

    }
})(AllDepartment);

const mapStateToProps = (state) => ({
    Auth: state.Auth,
    Departments: state.Department
});

const mapDispatchToProps = dispatch => ({
    RETRIEVE_ALL_DEPARTMENT: (token) => dispatch(RETRIEVE_ALL_DEPARTMENT(token)),
    REMOVE_DEPARTMENT_WITH_API: (token, departmentNumber) => dispatch(REMOVE_DEPARTMENT_WITH_API(token, departmentNumber)),
    FILTER_DEPARTMENT_BY_NAME: (filter, departmentCollection) => dispatch(FILTER_DEPARTMENT_BY_NAME(filter, departmentCollection))
});

export default connect(mapStateToProps, mapDispatchToProps)(AllDepartmentFormik);