import React, {useState} from "react";
import {NavLink} from "react-router-dom";
import styles from "../../styles/Navigation/Navigation.module.css";
import sideBarStyles from "../../styles/SideBar/SideBar.module.css";
import feather from "../../images/feather.png";
import {connect} from "react-redux";
import SideBar from "../SideBar/SideBar";
import {SIGNOUT} from "../../actions/Authentication/AuthAction";

const Navigation = props => {
    const [openStyle, setOpenStyle] = useState("");
    const [sideBarOpenStyle, setSideBarOpenStyle] = useState("");
    return (
        <div>
            <div className={styles.container}>
                <div className={styles.main}>
                    <div className={styles.navContainer}>
                        <div className={styles.navLeft}>
                            <NavLink to={"/"} className={styles.brand}><img src={feather}
                                                                            alt="feather"/>Feather</NavLink>
                            <NavLink to={"/about"}>About</NavLink>
                        </div>
                        <div className={styles.navRight}>
                            {!!props.Auth.accessToken ? (
                                <div>
                                    <NavLink to={"/employee/"}>Employees</NavLink>
                                    <NavLink to={"/employee/manager"}>Manager</NavLink>
                                    <NavLink to={"/department/"}>Departments</NavLink>
                                    <NavLink to={"/authentication/authLogs"}>Authentication Logs</NavLink>
                                    <NavLink to={"/pictures"}>Pictures</NavLink>
                                    <NavLink to={"/profile"}>Profile</NavLink>
                                </div>
                            ) : null}
                            {!!!props.Auth.accessToken ?
                                (<div>
                                    <NavLink to={"/auth/sign-up"}>Sign Up</NavLink>
                                    <NavLink to={"/auth/sign-in"}>Sign In</NavLink>
                                </div>)
                                :
                                (<div>
                                    <a onClick={() => {props.SIGNOUT()}} href="/auth/sign-in">Sign Out</a>
                                </div>)}
                        </div>
                        <div onClick={(event) => {
                            const openStyleCssClass = styles.open;
                            const sideBarOpenStyleCssClass = sideBarStyles.activateSideBar;
                            if (openStyle === "") {
                                setOpenStyle(openStyleCssClass);
                                setSideBarOpenStyle(sideBarOpenStyleCssClass);
                            }
                            else {
                                setOpenStyle("");
                                setSideBarOpenStyle("");
                            }
                        }} className={styles.hamburgerContainer + " " + openStyle}>
                            <div className={styles.hamburgerMain}></div>
                        </div>
                    </div>
                </div>
            </div>
            <SideBar openSideBar={sideBarOpenStyle}/>
        </div>
    )
};

const mapStateToProps = state => ({
    Auth: state.Auth
});

const mapDispatchToProps = dispatch => ({
    SIGNOUT: () => dispatch(SIGNOUT())
});

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);