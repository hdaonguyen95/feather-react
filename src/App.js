import React from "react";
import PublicRoute from "./routes/public-route";

function App() {
    return (
        <PublicRoute/>
    );
}

export default App;