const defaultState = [];

export default (state = defaultState, actions) => {
    switch (actions.type) {
        case "SET_PICTURES":
            return [...actions.pictures];
        case "ADD_PICTURE":
            return [...state, actions.picture];
        case "REMOVE_PICTURE":
            return state.filter(each => {
                return each.id !== actions.id
            });
        default:
            return state;
    }
};