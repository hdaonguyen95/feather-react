const defaultState = {
    accessToken: localStorage.getItem("accessToken") || ""
};

export default (state = defaultState, actions) => {
    switch (actions.type) {
        case "SIGN-IN":
            return {accessToken: actions.accessToken};
        case "SIGN-OUT":
            localStorage.clear();
            return {};
        default:
            return state;
    }
};