const defaultState = [];

export default (state = defaultState, actions) => {
    switch (actions.type) {
        case "SET_DEPARTMENTS":
            return [
                ...actions.departments
            ];
        case "ADD_DEPARTMENT":
            return [
                ...state,
                actions.department
            ]
        case "REMOVE_DEPARTMENT":
            return state.filter(({id}) => {
                return id !== actions.departmentNumber
            });
        case "UPDATE_DEPARTMENT":
            return state.map((department) => {
                if (department.id === actions.updates.id)
                    return {
                        ...department,
                        ...actions.updates
                    }
                else
                    return department;
            });
        default:
            return state;
    }
};