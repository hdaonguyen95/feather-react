const defaultState = [];

export default (state = defaultState, actions) => {
    switch (actions.type) {
        case "SET_EMPLOYEES":
            return [...actions.employees];
        case "ADD_EMPLOYEE":
            return [...state, actions.employee];
        case "REMOVE_EMPLOYEE":
            return state.filter((employee) => {
                return employee.id !== actions.id;
            });
        case "UPDATE_EMPLOYEE":
            return state.map((employee) => {
                if (employee.id === actions.updates.id) {
                    return {
                        ...employee,
                        ...actions.updates
                    };
                } else {
                    return employee;
                }
            });
        default:
            return state;
    }
};