import {applyMiddleware, combineReducers, createStore} from "redux";
import thunk from "redux-thunk";
import AuthReducer from "../reducers/Authentication/AuthReducer";
import DepartmentReducer from "../reducers/Department/DepartmentReducer";
import EmployeeReducer from "../reducers/Employee/EmployeeReducer";
import PictureReducer from "../reducers/Pictures/PictureReducer";

export default () => {
    return createStore(
        combineReducers({
            Auth: AuthReducer,
            Department: DepartmentReducer,
            Employee: EmployeeReducer,
            Picture: PictureReducer
        }), applyMiddleware(thunk)
    );
};